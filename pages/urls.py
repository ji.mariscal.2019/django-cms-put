"""items URL Configuration
"""

from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='index'),
    path('style', views.style),
    path('loggedIn', views.loggedIn),
    path('logOut', views.loggout_view),
    path('imagen', views.imagen_view),
    path('edit/<name>', views.edit, name='page'),
    path('<name>', views.page, name='page')
]
