from django import forms  # Importa la clase forms de Django

from .models import Page  # Importa el modelo Contenido del mismo directorio

class PageForm(forms.ModelForm):  # Define una clase de formulario para el modelo Contenido

    class Meta:  # Define la clase Meta para el formulario
        model = Page  # Especifica el modelo asociado al formulario
        fields = ('name', 'content',)  # Especifica los campos del modelo que se incluirán en el formulario

class PageContentForm(forms.ModelForm):  # Define una clase de formulario para el modelo Contenido

    class Meta:  # Define la clase Meta para el formulario
        model = Page  # Especifica el modelo asociado al formulario
        fields = ('content',)  # Especifica los campos del modelo que se incluirán en el formulario