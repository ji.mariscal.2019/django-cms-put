from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Page, Comment
from django.template import loader
from django.contrib.auth import logout
from django.utils import timezone
from django.shortcuts import redirect
from .forms import PageForm, PageContentForm
from django.shortcuts import get_object_or_404, render

html_template = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
  </head>
  <body>
    {body}
  </body>
</html>
"""
page_form = """
<p>
La pagina no existe. Crealá:
<p>
<form actions="" method="POST">
    Contenido de la Pagina: <input type="text" name="value">
   <br><input type="submit" name="action" value="Crear Página">
</form>
"""

edit_page_form = """
Quieres editar esta página: {name_page}
<p>
<form actions="" method="POST">
    Contenido de la Pagina: <input type="text" name="value">
   <br><input type="submit" name="action" value="Editar Página">
</form>
"""

comment_form = """
<p>
Crea un nuevo comentario:
<form actions="" method="POST">
    Título: <input type="text" name="title">
    <br>Contenido: <input type="text" name="body">
   <br/><input type="submit" name="action" value="Crear Comentario">
</form>
"""

html_item_template = "<li><a href='{name}'>{name}</a></li>"

page_content = """<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="utf-8" />
    <title>Django CMS</title>
    <link rel='stylesheet' type='text/css' href='main.css' />
  </head>
  <body>
    <b>Contenido de la Pagina:</b> {content}
    <p>
  </body>
</html>
"""


def index(request):
    pages = Page.objects.all()
    if len(pages) == 0:
        body = "No pages yet."
    else:
        body = "<ul>"
        for p in pages:
            body += html_item_template.format(name=p.name)
        body += "</ul>"
    return HttpResponse(html_template.format(body=body))


@csrf_exempt
def page(request, name):
    if request.method == 'PUT':
        try:
            p = Page.objects.get(name=name)
        except Page.DoesNotExist:
            p = Page(name=name)
        p.content = request.body.decode("utf-8")
        p.save()

    if request.method == 'POST':
        action = request.POST['action']

        if action == "Crear Comentario":
            p = Page.objects.get(name=name)
            title = request.POST["title"]
            body = request.POST["body"]
            date = timezone.now()
            com = Comment(page=p, title=title, body=body, date=date)
            com.save()

        elif action == "Crear Página":
            value = request.POST.get('value')
            try:
                p = Page.objects.get(name=name)
                p.content = value
            except Page.DoesNotExist:
                p = Page(name=name, content=value)
            p.save()

    if request.method == 'GET' or request.method == 'PUT' or request.method == 'POST':
        try:
            p = Page.objects.get(name=name)
            content = p.content
            comments = p.comment_set.all()
            response = page_content.format(content=content)
            for comment in comments:
                response += "<p><b>Comentario: </b> <br><b>Title</b>: " + comment.title + "<br><b>Cuerpo:</b> " + comment.body + "<br><b>Cuerpo:</b> " + str(comment.date)
            response += page_form + comment_form
            response = HttpResponse(response)
        except Page.DoesNotExist:
            if request.user.is_authenticated:
                response = HttpResponse(page_form)
            else:
                response = HttpResponse("No estás autenticado. <a href='/login'>Haz Loggin!</a>")
        return response
def home(request):
    content = Page.objects.all()
    template = loader.get_template('cms/index.html')
    context = {"page_list": content}
    return HttpResponse(template.render(context, request))

def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username + ". <a href='/'>Página Principal</a>"
    else:
        respuesta = "No estás autenticado"
    return HttpResponse(respuesta)

def loggout_view(request):
    logout(request)
    return HttpResponse("Loggout Made. <a href='/'>Página Principal</a>")

def imagen_view(request):
    template = loader.get_template('pages/imagen_template.html')
    context = {}
    return HttpResponse(template.render(context, request))

@csrf_exempt
def edit(request, name):
    if request.user.is_authenticated:
        try:
            p = Page.objects.get(name=name)
        except Page.DoesNotExist:
            p = None
        if request.method == 'POST':
            form = PageContentForm(request.POST, instance=p)
            if form.is_valid():
                form.save()
                return redirect('page', name=name)
        else:
            form = PageContentForm(instance=p)
        return render(request, 'cms/cms_edit.html', {'form': form})
    else:
        response = HttpResponse("No estás autenticado. <a href='/login'>Haz Loggin!</a>")
        return response

def style(request):
    text = """
    body {
    margin: 10px 20% 50px 70px;
    font-family: sans-serif;
    color: red;
    background: white;
    }
    """
    return HttpResponse(text)
