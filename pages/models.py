from django.db import models

#Cuando creas/modificas un modelo debes hacer makemigrations y migrate

class Page(models.Model):
    name = models.CharField(max_length=200)
    content = models.TextField()
    def __str__(self):
        return self.name

#Cada Página tiene  N comentarios
class Comment(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    title = models.CharField(max_length=128)
    body = models.TextField()
    date = models.DateTimeField()
    def __str__(self):
        return self.title