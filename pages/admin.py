from django.contrib import admin
from .models import Page, Comment

admin.site.register(Page)
admin.site.register(Comment)